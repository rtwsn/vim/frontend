import {ElGamal, ElGamalJSON} from "./elgamal";
import {modInv, modPow, randBetween} from "./utils";
import BN from 'bn.js';
import {Option, OptionJSON} from "./option";
import {Ballot, BallotJSON} from "./ballot";

export type SurveyJSON = {
    id: string
    question: string
    archived: boolean
    author: string
    created: number
    starts: string
    ends: string
    elgamal: ElGamalJSON,
    options: OptionJSON[],
    ballots: BallotJSON[],
    X: string,
    Y: string,
    result: string,
    combination?: number[],
}

export class Survey {

    id: string
    question: string
    archived: boolean
    author: string
    created: number
    starts: string
    ends: string
    elgamal: ElGamal
    options: Option[]
    ballots: Ballot[]
    X: BN
    Y: BN
    result: BN
    combination?: number[]

    constructor(
        id: string,
        question: string,
        archived: boolean,
        author: string,
        created: number,
        starts: string,
        ends: string,
        elgamal: ElGamal,
        options: Option[],
        ballots: Ballot[],
        X: BN,
        Y: BN,
        result: BN,
        combination?: number[]
    ) {
        this.id = id
        this.question = question
        this.archived = archived
        this.author = author
        this.created = created
        this.starts = starts
        this.ends = ends
        this.elgamal = elgamal
        this.options = options
        this.ballots = ballots
        this.X = X
        this.Y = Y
        this.result = result
        this.combination = combination
    }

    public static fromJSON(json: SurveyJSON): Survey {
        return new Survey(
            json.id,
            json.question,
            json.archived,
            json.author,
            json.created,
            json.starts,
            json.ends,
            ElGamal.fromJSON(json.elgamal),
            json.options.map(option => Option.fromJSON(option)),
            json.ballots.map(ballot => Ballot.fromJSON(ballot) as Ballot),
            new BN(json.X || 0),
            new BN(json.Y || 0),
            new BN(json.result || 0),
            json.combination || undefined,
        )
    }

    public async createBallot(option: number) {
        if (option < 0 || option >= this.options.length) {
            throw Error("Vote must be between 0 and " + this.options.length)
        }
        const g = this.elgamal.g
        const p = this.elgamal.p
        const q = this.elgamal.q
        const h = this.elgamal.public.h
        const G = this.options
        const vote = this.options[option].G
        const j = option
        // Start the vote
        const v = await this.elgamal.encrypt(vote)
        const x = v.x
        const y = v.y
        const r = v.r
        // Proof
        const a = (new Array(G.length)).fill(new BN(0))
        const b = (new Array(G.length)).fill(new BN(0))
        const c = (new Array(G.length)).fill(new BN(0))
        const z = (new Array(G.length)).fill(new BN(0))
        for (let i = 0; i < G.length; i++) {
            if (i === j) {
                continue
            }
            c[i] = await randBetween(new BN(0), q.sub(new BN(1)))
            z[i] = await randBetween(new BN(0), q.sub(new BN(1)))

            a[i] = modPow(g, z[i], p).mul(modPow(x, c[i], p)).mod(p)
            b[i] = modPow(h, z[i], p).mul(modPow(y.mul(modInv(G[i].G, p)), c[i], p)).mod(p)
        }
        const w = await randBetween(new BN(0), q.sub(new BN(1)))
        a[j] = modPow(g, w, p)
        b[j] = modPow(h, w, p)

        return new Ballot(this.id, j, x, y, a, b, c, z, r, w)
    }
}

