import BN from 'bn.js'
import {modPow, randBetween} from "./utils";

export type ElGamalJSON = {
    p: string,
    q: string,
    g: string,
    h: string
}

export class PrivateKey {

    public readonly p: BN
    public readonly q: BN
    public readonly g: BN
    public readonly x: BN

    constructor(p: BN, q: BN, g: BN, x: BN) {
        this.p = p
        this.q = q
        this.g = g
        this.x = x
    }

    public async decrypt(x: BN, y: BN) {
        const s = modPow(x, this.x, this.p)
        const m = s.invm(this.p).mul(y).mod(this.p)
        return m
    }
}

export class PublicKey {

    public readonly p: BN
    public readonly q: BN
    public readonly g: BN
    public readonly h: BN

    constructor(p: BN, q: BN, g: BN, h: BN) {
        this.p = p
        this.q = q
        this.g = g
        this.h = h
    }

    public async encrypt(m: BN) {
        const r = await randBetween(new BN(0), this.q.sub(new BN(1)))
        const x = modPow(this.g, r, this.p)
        const y = modPow(this.h, r, this.p).mul(m).mod(this.p)
        return {
            x, y, r,
        }
    }
}


export class ElGamal {

    public readonly p: BN
    public readonly q: BN
    public readonly g: BN
    public readonly private: PrivateKey
    public readonly public: PublicKey

    constructor(p: BN, q: BN, g: BN, s: BN, h: BN) {
        this.p = p
        this.q = q
        this.g = g
        this.private = new PrivateKey(p, q, g, s)
        this.public = new PublicKey(p, q, g, h)
    }

    public static fromJSON(json: ElGamalJSON): ElGamal {
        return new ElGamal(new BN(json.p), new BN(json.q), new BN(json.g), new BN(0), new BN(json.h))
    }

    public encrypt(m: BN) {
        return this.public.encrypt(m)
    }

    public decrypt(x: BN, y: BN) {
        return this.private.decrypt(x, y)
    }
}
