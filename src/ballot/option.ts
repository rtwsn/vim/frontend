import BN from 'bn.js'

export type OptionJSON = {
    G: string,
    data: any
}

export class Option {

    public data: any
    public readonly G: BN

    constructor(data: any, G: BN) {
        this.data = data
        this.G = G
    }

    public static fromJSON(json: OptionJSON): Option {
        return new Option(json.data, new BN(json.G))
    }
}
