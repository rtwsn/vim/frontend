import BN from 'bn.js'

/**
 * Generates a BNeger between min (inclusive) and max (exclusive)
 *
 * @param min Minimum number (inclusive)
 * @param max Maximum number (exclusive)
 * @returns {Promise<bitint>}
 */
export async function randBetween(min: BN, max: BN): Promise<BN> {
    const range = max.sub(min).sub(new BN(1))
    const bytes = Math.floor(range.bitLength() / 8)
    let x
    do {
        const arr = new Uint8Array(bytes)
        window.crypto.getRandomValues(arr)
        // buf[0] |= 0x80 // ensure first bit to be set
        x = bytesToBN(arr).add(min)
    } while (x.gte(max))
    return x
}

export function modPow(base: BN, exp: BN, mod: BN): BN {
    if (exp.eq(new BN(0))) return new BN(1);
    if (exp.mod(new BN(2)).eq(new BN(0))) {
        return modPow(base, exp.div(new BN(2)), mod).pow(new BN(2)).mod(mod);
    } else {
        return modPow(base, exp.sub(new BN(1)), mod).mul(base).mod(mod);
    }
}

export function modInv(a: BN, m: BN): BN {
    const r = a.egcd(m)
    if (!r.gcd.eq(new BN(1))) {
        throw Error('Modular inverse does not exist')
    }
    return r.a.mod(m)
}


export function bytesToBN(array: Uint8Array): BN {
    const hex: string[] = []
    array.forEach(i => {
        const h = i.toString(16)
        hex.push(h.length % 2 ? '0' + h : h)
    })
    return new BN(hex.join(''), 16)
}
