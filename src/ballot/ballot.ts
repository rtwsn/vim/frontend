import BN from 'bn.js'
import {Survey} from "./survey";

export type BallotJSON = {
    survey?: string,
    j?: number,
    x: string,
    y: string,
    a: string[],
    b: string[],
    c?: string[],
    z?: string[],
    r?: string,
    w?: string,
    C?: string,
    submitted?: boolean,
}

export class Ballot {

    protected survey: string
    public readonly j: number
    protected x: BN
    protected y: BN
    protected a: BN[]
    protected b: BN[]
    protected c: BN[]
    protected z: BN[]
    protected r: BN
    protected w: BN
    protected C: BN
    private submitted: boolean = false

    constructor(survey: string, j: number, x: BN, y: BN, a: BN[], b: BN[], c?: BN[], z?: BN[], r?: BN, w?: BN, C?: BN, submitted?: boolean) {
        this.survey = survey
        this.j = j
        this.x = x
        this.y = y
        this.a = a
        this.b = b
        this.c = c || []
        this.z = z || []
        this.r = r || new BN(0)
        this.w = w || new BN(0)
        this.C = C || new BN(0)
        this.submitted = submitted === true
    }

    public static fromJSON(json: BallotJSON): Ballot | undefined {
        if (json === undefined) {
            return undefined
        }
        const j = Number(json.j)
        return new Ballot(
            json.survey || '',
            j >= 0 ? j : -1,
            new BN(json.x),
            new BN(json.x),
            json.a.map(i => new BN(i)),
            json.b.map(i => new BN(i)),
            (json.c || []).map(i => new BN(i)),
            (json.z || []).map(i => new BN(i)),
            new BN(json.r || 0),
            new BN(json.w || 0),
            new BN(json.C || 0),
            json.submitted === true,
        )
    }

    public toJSON(step: 'submit' | 'challenge' | 'save' = 'submit'): BallotJSON {
        return {
            survey: step === 'save' ? this.survey : '',
            j: step === 'save' ? this.j : -1,
            x: this.x.toString(),
            y: this.y.toString(),
            a: step === 'save' || step === 'submit' ? this.a.map(i => i.toString()) : [],
            b: step === 'save' || step === 'submit' ? this.b.map(i => i.toString()) : [],
            c: step === 'save' || step === 'challenge' ? this.c.map(i => i.toString()) : [],
            z: step === 'save' || step === 'challenge' ? this.z.map(i => i.toString()) : [],
            r: step === 'save' ? this.r.toString() : '',
            w: step === 'save' ? this.w.toString() : '',
            C: step === 'save' ? this.C.toString() : '',
            submitted: step === 'save' ? this.submitted : false,
        }
    }

    public solveChallenge(survey: Survey, C: BN) {
        this.C = C
        const sum = this.c.reduce((p, c) => {
            return p.add(c)
        }, new BN(0))
        this.c[this.j] = this.C.sub(sum.sub(this.c[this.j])).umod(survey.elgamal.q)
        this.z[this.j] = this.w.sub(this.r.mul(this.c[this.j])).umod(survey.elgamal.q)
        return this
    }

    public setSubmitted() {
        this.submitted = true
    }

    public isSubmitted() {
        return !this.C.eq(new BN(0))
    }

    public isVerified() {
        return this.submitted
    }
}
