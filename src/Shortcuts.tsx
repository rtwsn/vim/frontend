import React, {Component} from 'react';
import "./Shortcuts.sass"
import {RouteComponentProps, withRouter} from "react-router";

interface ShortcutsAction {
    type: 'reset' | 'return' | 'message'
    payload?: any
}

interface ShortcutsState{
    inCommand: boolean
    inVisual: boolean
    command: string
    message?: string
    error?: string
}

interface ShortcutsProps extends RouteComponentProps {
    onVisualMode: () => void
    onVisualControl: (what: 'exit' | 'left' | 'right' | 'up' | 'down' | 'select') => void
}

export default class Shortcuts extends Component<ShortcutsProps, ShortcutsState>{

    private static COMMANDS: { [index: string]: (self: Shortcuts) => ShortcutsAction[] | void } = {
        v: (self: Shortcuts) => {
            self.setState({
                inCommand: false,
                inVisual: true,
            })
            self.props.onVisualMode()
            return [{type: 'return'}]
        },
        q: (self: Shortcuts) => {self.props.history.push('/logout')},
        help: (self: Shortcuts) => {
            console.log(`Help:

v   Enter visual mode
q   Log out

Within VISUAL:
h       Left
j       Down
k       Up
l       Right
Enter   Select
            `)
            return [{
                type: 'message',
                payload: 'See console for output'}]
        }
    }

    constructor(props: Readonly<ShortcutsProps>) {
        super(props)
        window.addEventListener('keypress', (e) => this.handleEvent(e))
        window.addEventListener('keyup', (e) => this.handleEvent(e))
    }

    componentWillMount() {
        this.reset()
    }

    private handleEvent(e: KeyboardEvent) {
        // console.log(e.target, e.key, this.state.inCommand, this.state.inVisual, this.state.command)
        if (e.target !== document.body) {
            return
        }
        const key = e.key
        if (e.type === 'keyup') {
            if (this.state.error === undefined && key === 'Enter') {
                if (this.state.inCommand) {
                    this.commit()
                } else if (this.state.inVisual) {
                    this.props.onVisualControl('select')
                }
            } else if (key === 'Escape') {
                if (this.state.error !== undefined) {
                    this.setState({
                        error: undefined,
                    })
                } else {
                    this.reset()
                }
            } else if (this.state.error === undefined && key === 'Backspace') {
                this.setState({
                    command: this.state.command.substr(0, this.state.command.length - 1),
                })
            }
        }
        if (e.type === 'keyup') {
            return
        }
        if (key.length > 1) {
            return
        }
        if ((!this.state.inCommand || this.state.error !== undefined || this.state.message !== undefined) && key === ':') {
            this.setState({
                inCommand: true,
                command: '',
                error: undefined,
                message: undefined,
            })
        } else if (this.state.inCommand) {
            this.setState({
                command: this.state.command.concat(key),
            })
        } else if (this.state.inVisual) {
            switch (e.code) {
                case 'KeyJ':
                    this.props.onVisualControl('down')
                    break
                case 'KeyK':
                    this.props.onVisualControl('up')
                    break
                case 'KeyH':
                    this.props.onVisualControl('left')
                    break
                case 'KeyL':
                    this.props.onVisualControl('right')
                    break
            }
        }
    }

    public render() {
        return <div className={`Shortcuts-container ${this.state.inCommand || this.state.inVisual ? 'active' : ''}`}>
            {
                this.state.error !== undefined ?
                    <span className="Shortcuts-error">{this.state.error}</span>
                    :
                    this.state.inVisual ?
                        <span className="Shortcuts-mode">VISUAL</span>
                        :
                        this.state.message !== undefined ?
                            <span className="Shortcuts-message">{this.state.message}</span>
                            :
                            <span className="Shortcuts-command">{':'.concat(this.state.command)}</span>
            }
        </div>
    }

    private reset() {
        this.props.onVisualControl('exit')
        this.setState({
            error: undefined,
            message: undefined,
            command: '',
            inCommand: false,
            inVisual: false,
        })
    }

    private commit() {
        if (!this.state.inCommand) {
            return
        }
        const commands = Object.keys(Shortcuts.COMMANDS)
        commands.sort((a, b) => a.length - b.length)
        const args = this.state.command.split(' ')
        let c = args.splice(0, 1)[0]
        for (let i = 0; i < commands.length; i++) {
            const idx = c.indexOf(commands[i])
            if (idx >= 0) {
                try {
                    const result = Shortcuts.COMMANDS[commands[i]](this)
                    if (Array.isArray(result)) {
                        for (let j = 0; j < result.length; j++) {
                            const action = result[j]
                            if (typeof action !== typeof {}) {
                                continue
                            }
                            switch (action.type) {
                                case 'reset':
                                    this.reset()
                                    break
                                case 'message':
                                    this.setState({
                                        message: action.payload,
                                    })
                                    break
                                case 'return':
                                    return
                            }
                        }
                    }
                } catch (e) {
                    this.setState({
                        command: '',
                        error: e.message,
                    })
                    return
                }
                c = c.substr(0, idx).concat(c.substr(idx + commands[i].length))
            }
        }
        if (c !== '') {
            this.setState({
                error: `Unknown command "${c}"`
            })
        } else {
            this.reset()
        }
    }
}

export const ShortcutsWithRouter = withRouter(Shortcuts)
