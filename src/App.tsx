import React, {Component, createRef} from 'react';
import {Route, Switch, NavLink, BrowserRouter} from 'react-router-dom';
import './App.sass';
import logo from './assets/logo.png'
import {BallotModel, Survey} from "./views/survey/Survey";
import {Surveys} from "./views/survey/Surveys";
import {Login} from "./views/auth/Login";
import {Register} from "./views/auth/Register";
import {Provider} from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'
import {configureStore} from "./store";
import {PrivateRoute} from "./views/auth/PrivateRoute";
import {Activate} from "./views/auth/Activate";
import ApiService from "./services/api";
import {Home} from "./views/Home";
import {Logout} from "./views/auth/Logout";
import {Settings} from "./views/Settings";
import {ShortcutsWithRouter} from "./Shortcuts";
import Fader from 'react-fader'
import {AddSurvey, SurveyCreation} from "./views/survey/AddSurvey";

const { store, persistor } = configureStore()

;(window as any).store = store

const apiService = new ApiService('http://localhost:8000', store)

interface AppState {
  loggedIn: boolean
  theme: string
  visualMode: boolean
  selected: number
  selectionMax: number
}

interface AppProps {
}

export class App extends Component<AppProps, AppState> {

  private selectionRef = createRef<any>()

  constructor(props: Readonly<AppProps>) {
    super(props)
    this.onVisualMode = this.onVisualMode.bind(this)
    this.onVisualControl = this.onVisualControl.bind(this)
    this.onSetSelection = this.onSetSelection.bind(this)
  }

  updateState() {
    this.setState({
      loggedIn: store.getState().auth.loggedIn,
      theme: store.getState().app.theme,
      visualMode: false,
      selected: 0,
      selectionMax: -1,
    })
  }

  componentWillMount() {
    this.updateState()
    store.subscribe(() => {
      this.updateState()
    })
  }

  onVisualMode() {
    this.setState({
      visualMode: true,
      selected: 0,
    })
  }

  onVisualControl(what: 'exit' | 'left' | 'right' | 'up' | 'down' | 'select') {
    if (what === 'exit') {
      this.setState({
        selected: -1,
      })
    } else if (what === 'left') {
      this.selectionRef.current && this.selectionRef.current.onVisualLeft && this.selectionRef.current.onVisualLeft()
    } else if (what === 'right') {
      this.selectionRef.current && this.selectionRef.current.onVisualRight && this.selectionRef.current.onVisualRight()
    } else if (what === 'up') {
      this.setState({
        selected: Math.max(0, this.state.selected - 1)
      })
    } else if (what === 'down') {
      this.setState({
        selected: Math.min(this.state.selectionMax, this.state.selected + 1)
      })
    } else if (what === 'select') {
      this.selectionRef.current && this.selectionRef.current.onVisualSelect && this.selectionRef.current.onVisualSelect(this.state.selected)
    }
  }

  onSetSelection(max: number) {
    this.setState({
      selectionMax: max,
      selected: 0,
    })
  }

  render() {
    return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
              <div className={`theme-${this.state.theme}`}>
                <div className="App">
                  <header className="App-header">
                    <NavLink to="/">
                      <img src={logo} alt="Logo" />
                      <h1>V<small>ote</small>I<small>ncognito</small>M<small>achine</small></h1>
                    </NavLink>
                    <div className="spacer"></div>
                    <ul>
                      {/*<li><NavLink exact to="/">Home</NavLink></li>*/}
                      <li><NavLink to="/survey">Surveys</NavLink></li>
                      <li><NavLink to="/settings">Settings</NavLink></li>
                      <li>
                        { this.state.loggedIn ?
                            <NavLink exact to="/logout">Logout</NavLink>
                            :
                            <NavLink exact to="/login">Login</NavLink>
                        }
                      </li>
                    </ul>
                  </header>
                  { <ShortcutsWithRouter onVisualMode={this.onVisualMode} onVisualControl={this.onVisualControl} /> }
                  <div className="App-wrapper">
                    <div className="App-container">
                      <div className="App-content">
                        <Route render={({ location }: any) => (
                            <Fader>
                                <Switch location={location} key={location.key}>
                                  <Route path="/" exact component={Home} key="home" />
                                  <Route path="/login" exact render={(props: any) => <Login onLogin={onLogin} history={props.history} />} key="login" />
                                  <Route path="/logout" exact render={(props: any) => <Logout onLogout={onLogout} history={props.history} />} key="logout" />
                                  <Route path="/activate/:token" exact component={Activate} key="activate" />
                                  <Route path="/register" exact component={Register} key="register" />
                                  <Route path="/settings" exact render={(props: any) => <Settings store={store} />} key="settings" />
                                  <PrivateRoute path="/survey/" exact render={(props: any) => <Surveys onListSurveys={onListSurveys} history={props.history} ref={this.selectionRef} onSetSelection={this.onSetSelection} visualMode={this.state.visualMode} selected={this.state.selected} />} key="survey_list" />
                                  <PrivateRoute path="/survey/add" exact render={(props: any) => <AddSurvey onCreateSurvey={onCreateSurvey} history={props.history} />} key="survey_create" />
                                  <PrivateRoute path="/survey/:id" exact render={(props: any) => <Survey onGetSurvey={onGetSurvey} onSubmitVote={onSubmitVote} onSolveChallenge={onSolveChallenge} onTallySurvey={onTallySurvey} onArchiveSurvey={onArchiveSurvey} history={props.history} match={props.match} ref={this.selectionRef} onSetSelection={this.onSetSelection} visualMode={this.state.visualMode} selected={this.state.selected} />} />} key="survey_view" />
                                  <Route component={RouteNotFound} key="404" />
                                </Switch>
                            </Fader>
                        )}/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </BrowserRouter>
          </PersistGate>
        </Provider>
    );
  }
}

function onLogin(email: string, password: string) {
  return apiService.login(email, password)
}

function onLogout() {
  return apiService.logout()
}

function onListSurveys(page: number, archived: boolean, ended: boolean, future: boolean) {
  return apiService.listSurveys(page, archived, ended, future)
}

function onGetSurvey(id: string) {
  return apiService.getSurvey(id)
}

function onSubmitVote(id: string, ballot: BallotModel) {
  return apiService.submitVote(id, ballot)
}

function onSolveChallenge(id: string, ballot: BallotModel) {
  return apiService.solveChallenge(id, ballot)
}

function onCreateSurvey(survey: SurveyCreation) {
  return apiService.createSurvey(survey)
}

function onTallySurvey(id: string) {
  return apiService.tallySurvey(id)
}

function onArchiveSurvey(id: string) {
  return apiService.archiveSurvey(id)
}

const RouteNotFound: React.FC = ({ location }: any) => {
  return <h1>404 <code>{location.pathname}</code></h1>;
}

export const Loading: React.SFC<{ children?: any }> = (props) => {
  return <div className="App-loading">
    <div className="Loading-content"><div className="Loading-indicator"></div>{props.children}</div>
  </div>;
}

export default App
