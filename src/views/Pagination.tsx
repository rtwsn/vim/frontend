import React, {Component} from "react";
import "./Pagination.sass"

type PaginationState = {}

type PaginationProps = {
    onSelect: (page: number) => void
    page: number
    total: number
    page_size: number
}

export class Pagination extends Component<PaginationProps, PaginationState> {

    constructor(props: Readonly<PaginationProps>) {
        super(props)
        this.onSelect = this.onSelect.bind(this)
    }

    componentWillMount() {
        this.setState({
            page: 0,
        })
    }

    componentDidMount() {
    }

    onSelect(page: number) {
        if (page < 0 || page >= Math.floor(this.props.total / this.props.page_size)) {
            return
        }
        this.props.onSelect(page)
    }

    range(maximum: number = 10) {
        maximum = Math.max(maximum, 3)
        const pages = Math.max(1, Math.floor(this.props.total / this.props.page_size))
        const elements = Math.min(maximum, pages)
        let range: any[] = [this.props.page]
        for (let i = this.props.page - 1; i >= 0 && i > this.props.page - Math.floor(elements / 2) - 1; i--) {
            range.push(i)
        }
        let hasBefore = false
        if (range.indexOf(0) === -1) {
            range = range.slice(0, -2).concat([0])
            hasBefore = true
        }
        for (let i = this.props.page + 1; i < pages && range.length < elements; i++) {
            range.push(i)
        }
        range = range.sort((a, b) => a - b)
        if (hasBefore) {
            for (let i = range[1] - 1; i >= 0 && range.length < maximum - 1; i--) {
                if (i === 0) {
                    hasBefore = false
                    break
                }
                range.splice(1, 0, i)
            }
        }
        let hasAfter = false
        if (range.indexOf(pages - 1) === -1) {
            range = range.slice(0, -2).concat([pages - 1])
            hasAfter = true
        }
        if (hasBefore) {
            range.splice(1, 0, '...')
        }
        if (hasAfter) {
            range.splice(range.length - 1, 0, '...')
        }
        return range
    }

    render() {
        return <div className="Pagination-container">
            <ul className="Pagination-list">
                <li className={`Pagination-listitem btn btn-submit ${this.props.page === 0 ? 'disabled' : ''}`} onClick={() => {this.onSelect(this.props.page - 1)}}>&lt;</li>
                {this.range().map((page, i) => {
                    return <li className={`Pagination-listitem btn btn-submit ${page === '...' ? 'disabled': ''} ${page === this.props.page ? 'active': ''}`}  onClick={() => page !== '...' && this.onSelect(page)} key={i}>{page === '...' ? page : page + 1}</li>
                })}
                <li className={`Pagination-listitem btn btn-submit ${this.props.page === Math.max(0, Math.floor(this.props.total / this.props.page_size) - 1) ? 'disabled' : ''}`} onClick={() => {this.onSelect(this.props.page + 1)}}>&gt;</li>
            </ul>
        </div>;
    }
}
