import React, {Component} from "react";
import {Loading} from "../../App";
import * as H from "history";

interface LogoutState {
    error: boolean
}

interface LogoutProps {
    history: H.History
    onLogout: () => Promise<boolean>
}

export class Logout extends Component<LogoutProps, LogoutState> {
    mounted: boolean = false

    constructor(props: Readonly<LogoutProps>) {
        super(props)
        this.onRetry = this.onRetry.bind(this)
    }

    componentWillMount() {
        this.setState({
            error: false
        })
    }

    componentDidMount() {
        this.mounted = true
        this.logout()
    }

    componentWillUnmount() {
        this.mounted = false
    }

    logout(count: number = 1) {
        if (count > 3) {
            return this.setState({
                error: true
            })
        }
        this.props.onLogout().then(loggedOut => {
            if (loggedOut) {
                this.props.history.push("/")
            } else if (this.mounted) {
                setTimeout(() => {this.logout(count + 1)}, 1000)
            }
        })
    }

    onRetry() {
        this.setState({
            error: false
        })
        this.logout()
    }

    render() {
        return <div>
            <h2>Logout</h2>
            { this.state.error ?
                <div>
                    <div className="warn">An error occured!</div>
                    <button className="btn btn-submit" onClick={this.onRetry}>Retry?</button>
                </div>
                :
                <Loading/>
            }
        </div>;
    }
}
