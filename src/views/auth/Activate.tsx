import React, {Component} from "react";
import {RouteComponentProps} from "react-router";

type ActivateState = {
    token: string
    loading: boolean
}

export class Activate extends Component<RouteComponentProps<{ token: string }>, ActivateState> {

    componentWillMount() {
        this.setState({
            token: this.props.match.params.token,
            loading: true,
        })
    }

    componentDidMount() {
    }

    render() {
        return <div>
            {
                this.state.loading ?
                    <div> loading </div>
                    :
                    <div> done </div>
            }
        </div>
    }
}
