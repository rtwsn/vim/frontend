import React, {ChangeEvent, Component} from "react";
import {Loading} from "../../App";
import * as H from "history";
import "./Login.sass"

type LoginState = {
    email: string,
    password: string,
    loading: boolean,
}

interface LoginProps {
    history: H.History
    onLogin: (email: string, password: string) => Promise<boolean>
}

export class Login extends Component<LoginProps, LoginState> {

    constructor(props: Readonly<LoginProps>) {
        super(props)
        this.setEmail = this.setEmail.bind(this)
        this.setPassword = this.setPassword.bind(this)
        this.login = this.login.bind(this)
    }

    componentWillMount() {
        this.setState({
            email: 'user@example.com',
            password: 'Abc!23xyz',
        });
    }

    componentDidMount() {
    }

    setEmail(event: ChangeEvent) {
        this.setState({
            email: (event.target as HTMLInputElement).value,
        })
    }

    setPassword(event: ChangeEvent) {
        this.setState({
            password: (event.target as HTMLInputElement).value,
        })
    }

    login() {
        this.setState({
            loading: true,
        })
        this.props.onLogin(this.state.email, this.state.password).then(loggedIn => {
            this.setState({
                loading: false,
            })
            if (loggedIn) {
                this.setState({
                    email: '',
                    password: '',
                })
                this.props.history.push("/survey")
            }
        })
    }

    render() {
        return <div className="Login-container">
            { this.state.loading && <Loading/>}
            <h2>Login</h2>
            <div className="form Login-form">
                <div className="group">
                    <input type="text" required value={this.state.email} onChange={this.setEmail} />
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <label>Email address</label>
                </div>
                <div className="group">
                    <input type="password" required value={this.state.password} onChange={this.setPassword} />
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <label>Password</label>
                </div>
                <button className="btn btn-submit" onClick={() => {this.login()}}>Login</button>
            </div>
        </div>;
    }
}
