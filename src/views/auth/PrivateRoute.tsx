import React from "react";
import {Redirect, Route} from "react-router";
import {ReactReduxContext} from "react-redux";
import {Store} from "redux";
import {ApplicationState} from "../../store";

export function PrivateRoute({ component: Component, render, ...rest }: any) {
    return (
        <ReactReduxContext.Consumer>
            {({ store }: { store: Store<ApplicationState> }) => <Route
                {...rest}
                render={
                    props => store.getState().auth.loggedIn ? (
                        render !== undefined ? render(props) : <Component {...props} />
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: props.location }
                            }}
                        />
                    )
                }
            />}
        </ReactReduxContext.Consumer>
    );
}
