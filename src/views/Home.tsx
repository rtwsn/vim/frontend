import React from "react";
import {Link} from "react-router-dom";
import logo from "../assets/logo.png";
import "./Home.sass"


export const Home = () => {
    return <div className="Home-container">
        <h2>Home</h2>
        <p>Welcome to VIM. If you already signed up for an account, go to <Link to="/login">login</Link>.</p>
        <p>If you didn't already register yourself, please go to the <Link to="/register">registration</Link> page.</p>
        <p>If you have any questions about the tool, don't hesitate to drop me an email or open an issue in the <a href="/">issue tracker</a>.</p>
        <img src={logo} alt="Logo" />
    </div>;
}
