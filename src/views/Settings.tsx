import React, {ChangeEvent, Component} from "react";
import {ReactReduxContext} from "react-redux";
import {Store, Unsubscribe} from "redux";
import {ApplicationState} from "../store";
import {changeLanguage, changeTheme} from "../store/app/actions";
import "./Settings.sass"

interface SettingsState {
    language: string
    theme: string
}

interface SettingsProps {
    store: Store<ApplicationState>
}

export class Settings extends Component<SettingsProps, SettingsState> {
    private static LANGUAGES: { value: string, name: string }[] = [
        { value: 'en', name: 'English' },
        { value: 'de', name: 'Deutsch' },
    ]
    private static THEMES: { value: string, name: string }[] = [
        { value: 'dark', name: 'Dark' },
        { value: 'light', name: 'Light' },
    ]

    private unsubscribe?: Unsubscribe

    constructor(props: Readonly<SettingsProps>) {
        super(props)
        this.onSelectLanguage = this.onSelectLanguage.bind(this)
        this.onSelectTheme = this.onSelectTheme.bind(this)
    }

    componentWillMount() {
        this.unsubscribe = this.props.store.subscribe(() => {
            const state = this.props.store.getState()
            this.setState({
                language: state.app.language,
                theme: state.app.theme,
            })
        })
    }

    componentWillUnmount() {
        if (this.unsubscribe !== undefined) {
            this.unsubscribe()
        }
    }

    onSelectLanguage(event: ChangeEvent) {
        const language = (event.target as HTMLInputElement).value
        if (language !== 'en' && language !== 'de') {
            return
        }
        this.props.store.dispatch(changeLanguage(language))
    }

    onSelectTheme(event: ChangeEvent) {
        const theme = (event.target as HTMLInputElement).value
        if (theme !== 'dark' && theme !== 'light') {
            return
        }
        this.props.store.dispatch(changeTheme(theme))
    }

    render() {
        return (
            <ReactReduxContext.Consumer>
                {({store}: { store: Store<ApplicationState> }) => <div className="Settings-container">
                    <h2>Settings</h2>
                    <div className="Settings-grid">
                        <div className="Settings-description">Language (currently not implemented)</div>
                        <div className="Settings-value">
                            <select onChange={this.onSelectLanguage} value={this.props.store.getState().app.language}>
                                {Settings.LANGUAGES.map((language, i) => {
                                    return <option value={language.value} key={i}>{language.name}</option>
                                })}
                            </select>
                        </div>
                        <div className="Settings-spacer"></div>
                        <div className="Settings-description">Theme</div>
                        <div className="Settings-value">
                            <select onChange={this.onSelectTheme} value={this.props.store.getState().app.theme}>
                                {Settings.THEMES.map((theme, i) => {
                                    return <option value={theme.value} key={i}>{theme.name}</option>
                                })}
                            </select>
                        </div>
                    </div>
                </div>}
            </ReactReduxContext.Consumer>
        );
    }
}
