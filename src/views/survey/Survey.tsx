import {match} from "react-router";
import React, {Component, CSSProperties} from "react";
import "./Survey.sass"
import {AxiosError, AxiosResponse} from "axios";
import * as H from "history";
import {Survey as CryptoSurvey} from "../../ballot/survey"
import BN from 'bn.js';
import Dexie from 'dexie';
import {Ballot, BallotJSON} from "../../ballot/ballot";
import {Store} from "redux";
import {ApplicationState} from "../../store";
import {ReactReduxContext} from "react-redux";

interface IBallot {
    survey: string
    j: number
    x: string
    y: string
    a: string[]
    b: string[]
    c: string[]
    z: string[]
    r: string
    w: string
    C: string
    submitted: boolean
}

class SurveyDatabase extends Dexie {
    ballots: Dexie.Table<IBallot, string>;

    constructor () {
        super("SurveyDatabase");
        this.version(1).stores({
            ballots: '++survey, j, x, y, a, b, c, z, r, w, C, submitted',
        });
        this.ballots = this.table("ballots");
    }

    save(ballot?: Ballot) {
        if (ballot === undefined) {
            return undefined
        }
        return this.ballots.put(ballot.toJSON('save') as IBallot)
    }

    async status(survey: string) {
        return Ballot.fromJSON(await this.ballots.get(survey) as BallotJSON)
    }
}

interface ElgamalModel {
    p: string
    q: string
    g: string
    h: string
}

interface OptionModel {
    G: string
    data: {
        title: string
    }
}

export interface BallotModel {
    x: string
    y: string
    a: string[]
    b: string[]
    C?: string
    c?: string[]
    z?: string[]
}

interface SurveyModel {
    id: string
    question: string
    archived: boolean
    author: string
    created: number
    starts: string
    ends: string
    elgamal: ElgamalModel
    options: OptionModel[]
    ballots: BallotModel[]
    X: string
    Y: string
    result: string
    combination: number[]
}


type SurveyState = {
    loading: boolean
    error?: string
    survey?: CryptoSurvey
    ballot?: Ballot
    showVote: boolean
}

type SurveyProps = {
    history: H.History
    match: match<{ id: string }>
    onGetSurvey: (id: string) => Promise<AxiosResponse<SurveyModel>>
    onSubmitVote: (id: string, ballot: BallotModel) => Promise<AxiosResponse<{ C: string }>>
    onSolveChallenge: (id: string, ballot: BallotModel) => Promise<AxiosResponse<{}>>
    onTallySurvey: (id: string) => Promise<AxiosResponse<SurveyModel>>
    onArchiveSurvey: (id: string) => Promise<AxiosResponse<SurveyModel>>
    onSetSelection: (max: number) => void
    visualMode: boolean
    selected: number
}

export class Survey extends Component<SurveyProps, SurveyState> {
    private db: SurveyDatabase

    constructor(props: Readonly<SurveyProps>) {
        super(props)
        this.onVote = this.onVote.bind(this)
        this.onTally = this.onTally.bind(this)
        this.onArchive = this.onArchive.bind(this)
        this.db = new SurveyDatabase()
        this.props.onSetSelection(-1)
    }

    componentWillMount() {
        this.setState({
            loading: false,
            error: undefined,
            survey: undefined,
            ballot: undefined,
            showVote: false,
        })
    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        this.props.onGetSurvey(this.props.match.params.id).then(async result => {
            this.onLoadSurvey(result.data)
        }).catch((err: AxiosError) => {
            this.setState({
                loading: false,
                error: err.message,
            })
        })
    }

    async onLoadSurvey(survey: SurveyModel) {
        this.setState({
            loading: false,
            survey: CryptoSurvey.fromJSON(survey),
            ballot: await this.db.status(survey.id),
        })
        this.props.onSetSelection(survey.options.length - 1)
    }

    onVisualSelect(i: number) {
        this.onVote(i)
    }

    async onVote(j: number) {
        if (this.state.survey === undefined || this.state.survey.options[j] === undefined) {
            return
        }
        if (!window.confirm(`Are you sure you wanna vote for "${this.state.survey.options[j].data.title}"? At the moment, submited votes cannot be changed!`)) {
            return
        }
        let ballot = this.state.ballot
        if (ballot === undefined) {
            console.log('new ballot')
            ballot = await this.state.survey.createBallot(j)
        }
        console.log(ballot.j)
        if (ballot.j !== j) {
            if (!ballot.isSubmitted()) {
                ballot = await this.state.survey.createBallot(j)
            } else {
                alert('Chaning your vote is currently unsupported')
                this.setState({
                    loading: false,
                })
                return
            }
        } else if (ballot.isVerified()) {
            return
        }
        this.setState({
            loading: true,
        })
        await this.db.save(ballot)
        this.setState({
            ballot: ballot,
        })
        // TODO: Save ballot to some storage before sending request
        console.log(ballot.isSubmitted(), ballot.isVerified())
        if (!ballot.isSubmitted()) {
            try {
                const submitResult = await this.props.onSubmitVote((this.state.survey as CryptoSurvey).id, (ballot as Ballot).toJSON('submit'))
                ;(ballot as Ballot).solveChallenge(this.state.survey as CryptoSurvey, new BN(submitResult.data.C))
                await this.db.save(ballot)
                this.setState({
                    ballot: ballot,
                })
            } catch (e) {
                const err: AxiosError = e
                if (err.response && err.response.status === 404) {
                    this.setState({
                        error: 'Survey not found or already over!',
                    })
                }
                if (err.response && err.response.status === 400) {
                    this.setState({
                        error: 'Already voted!',
                    })
                }
                this.setState({
                    loading: false,
                })
                return
            }
        }
        if (!ballot.isVerified()) {
            try {
                await this.props.onSolveChallenge((this.state.survey as CryptoSurvey).id, (ballot as Ballot).toJSON('challenge'))
                ;(ballot as Ballot).setSubmitted()
                await this.db.save(ballot)
                this.setState({
                    ballot: ballot,
                })
            } catch (e) {
                const err: AxiosError = e
                if (err.response && err.response.status === 422) {
                    this.setState({
                        error: 'Challenge was not solved correctly!',
                    })
                }
            }
        }
        this.setState({
            loading: false,
            ballot: ballot
        })
    }

    onTally() {
        this.props.onTallySurvey(this.state.survey!.id).then(result => {
            this.onLoadSurvey(result.data)
        }).catch((err: AxiosError) => {
            if (err.response && err.response.status === 400) {
                this.setState({
                    error: 'Survey is not yet finished!',
                })
            }
            this.setState({
                loading: false,
            })
        })
    }

    onArchive() {
        this.props.onArchiveSurvey(this.state.survey!.id).then(result => {
            this.props.history.push('/survey')
        }).catch((err: AxiosError) => {
            if (err.response && err.response.status === 400) {
                this.setState({
                    error: 'Couldn\'t archive survey!',
                })
            }
            this.setState({
                loading: false,
            })
        })
    }

    private isMax(combination: number[], i: number) {
        return combination[i] === Math.max(...combination)
    }

    private leadingZeros(s: number | string, length: number = 2) {
        return '0'.repeat(length).concat(s as string).substr(-2)
    }

    private getToday() {
        const now = new Date()
        return `${now.getFullYear()}-${this.leadingZeros(now.getMonth() + 1)}-${this.leadingZeros(now.getDate())}`
    }

    render() {
        return <ReactReduxContext.Consumer>
            {({ store }: { store: Store<ApplicationState> }) => <div className="Survey-container">
                <h2>Survey</h2>
                {
                    this.state.loading ?
                        <div className="Loading-content"><div className="Loading-indicator"></div></div>
                        :
                        this.state.error !== undefined ?
                            <div className="warn">{ this.state.error }</div>
                            :
                            this.state.survey === undefined ?
                                <div className="info">No survey loaded (yet)...</div>
                                :
                                <div className="Survey-details">
                                    <div className="Survey-controls">
                                        {
                                            this.state.survey!.combination !== undefined &&
                                            this.state.survey!.author === store.getState().auth.email &&
                                            this.state.survey!.archived === false &&
                                            <button className="btn btn-submit" onClick={this.onArchive}>Archive survey</button>
                                        }
                                        {
                                            this.state.survey!.combination === undefined && this.state.survey!.ends <= this.getToday() &&
                                            <button className="btn btn-submit" onClick={this.onTally}>Tally survey</button>
                                        }
                                    </div>
                                    <h3>{ this.state.survey!.question }</h3>
                                    <h4>Open from {this.state.survey!.starts} until {this.state.survey!.ends} (exclusive)</h4>
                                    {
                                        this.state.survey!.combination !== undefined ?
                                            [
                                                <div className="info" key="finished_info">This survey already finished. The results are shown below.</div>,
                                                <ul key="results_list" className="Survey-results" style={{'--survey-total': `${this.state.survey!.ballots.length}`} as CSSProperties}>
                                                    {this.state.survey!.options.map((option, i) => {
                                                        return <li className={`Survey-result ${this.isMax(this.state.survey!.combination!, i) ? 'winner' : ''}`} key={i} style={{'--survey-count': this.state.survey!.combination![i]} as CSSProperties}>
                                                            <label>{`${option.data.title}: ${this.state.survey!.combination![i]}`}</label>
                                                            <div className="Survey-result-bar"><div></div></div>
                                                        </li>
                                                    })}
                                                </ul>
                                            ]
                                            :
                                            [
                                                this.state.showVote === false && this.state.ballot !== undefined && this.state.ballot.isVerified() &&
                                                    <button key="show_vote" className="btn btn-submit" onClick={() => {this.setState({ showVote: true })}}>Show your current vote</button>,
                                                <ul key="options_list" className="Survey-list">
                                                    {this.state.survey!.options.map((option, i) => {
                                                        return <li className={`Survey-listitem ${this.state.showVote && this.state.ballot && this.state.ballot.j === i ? 'active' : ''} ${this.props.visualMode && this.props.selected === i ? 'selected' : ''}`} onClick={() => this.onVote(i)}
                                                                   key={i}>{option.data.title}</li>
                                                    })}
                                                </ul>
                                            ]
                                    }
                                </div>
                }
            </div>}
        </ReactReduxContext.Consumer>;
    }
}
