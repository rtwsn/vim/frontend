import React, {ChangeEvent, Component, CSSProperties} from "react";
import "./AddSurvey.sass"
import {AxiosError, AxiosResponse} from "axios";
import * as H from "history";
import {Loading} from "../../App";

export interface SurveyCreation {
    question: string
    starts: string
    ends: string
    options: { title: string }[]
}


type AddSurveyState = {
    loading: boolean
    error?: string
    question: string
    starts: string
    ends: string
    options: { title: string }[]
}

type AddSurveyProps = {
    history: H.History
    onCreateSurvey: (survey: SurveyCreation) => Promise<AxiosResponse<{
        id: string
    }>>
}

export class AddSurvey extends Component<AddSurveyProps, AddSurveyState> {

    constructor(props: Readonly<AddSurveyProps>) {
        super(props)
        this.onCreate = this.onCreate.bind(this)
        this.onChangeQuestion = this.onChangeQuestion.bind(this)
        this.onChangeStarts = this.onChangeStarts.bind(this)
        this.onChangeEnds = this.onChangeEnds.bind(this)
        this.onAddQuestion = this.onAddQuestion.bind(this)
        this.onChangeOption = this.onChangeOption.bind(this)
        this.onDeleteQuestion = this.onDeleteQuestion.bind(this)
    }

    componentWillMount() {
        this.setState({
            loading: false,
            error: undefined,
            question: '',
            starts: '',
            ends: '',
            options: [
                { title: '' },
                { title: '' },
            ]
        })
    }

    componentDidMount() {
    }

    onCreate() {
        if (this.state.question.length < 10) {
            this.setState({
                error: 'Question must be at least 10 characters',
            })
            return
        }
        if (this.state.starts === '') {
            this.setState({
                error: 'Start must be set',
            })
            return
        }
        if (this.state.ends === '') {
            this.setState({
                error: 'End must be set',
            })
            return
        }
        for (let i = 0; i < this.state.options.length; i++) {
            if (this.state.options[i].title === '') {
                this.setState({
                    error: 'Option text may notbe empty',
                })
                return
            }
        }
        this.setState({
            loading: true,
            error: undefined,
        })
        this.props.onCreateSurvey({
            question: this.state.question,
            starts: this.state.starts,
            ends: this.state.ends,
            options: this.state.options,
        }).then(async result => {
            this.setState({
                loading: false,
            })
            this.props.history.push(`/survey/${result.data.id}`)
        }).catch((err: AxiosError) => {
            this.setState({
                loading: false,
                error: err.message,
            })
        })
    }

    onChangeQuestion(event: ChangeEvent) {
        this.setState({
            question: (event.target as HTMLInputElement).value,
        })
    }

    onChangeStarts(event: ChangeEvent) {
        this.setState({
            starts: (event.target as HTMLInputElement).value,
        })
    }

    onChangeEnds(event: ChangeEvent) {
        this.setState({
            ends: (event.target as HTMLInputElement).value,
        })
    }

    onAddQuestion() {
        this.setState({
            options: [...this.state.options, { title: '' }],
        })
    }

    onDeleteQuestion(i: number) {
        if (this.state.options.length <= 2) {
            this.setState({
                error: 'At least two options must be given.'
            })
            return
        }
        this.setState({
            options: [...this.state.options.slice(0, i), ...this.state.options.slice(i + 1)],
        })
    }

    onChangeOption(i: number, event: ChangeEvent) {
        const options = this.state.options
        options[i] = { title: (event.target as HTMLInputElement).value }
        this.setState({
            options: options
        })
    }

    render() {
        return <div className="AddSurvey-container">
            <h2>Create Survey</h2>
            {
                this.state.loading && <Loading>Survey creation may take some time</Loading>
            }
            {
                this.state.error && <div className="warn">{this.state.error}</div>
            }
            <div className="form">
                <div className="group">
                    <input value={this.state.question} required onChange={this.onChangeQuestion} />
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <label>Question</label>
                </div>
                <div className="group">
                    <input value={this.state.starts} required type="date" onChange={this.onChangeStarts} />
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <label>Starts</label>
                </div>
                <div className="group">
                    <input value={this.state.ends} required type="date" onChange={this.onChangeEnds} />
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <label>Ends</label>
                </div>
                <ul className="AddSurvey-options">
                    {this.state.options.map((option, i) => {
                        return <li className="AddSurvey-option" key={i}>
                            <div className="group">
                                <input value={option.title} required onChange={(e) => this.onChangeOption(i, e)} />
                                <span className="highlight"></span>
                                <span className="bar"></span>
                                <label>Option {i + 1}</label>
                            </div>
                            <div className="button">
                                <button className="AddSurvey-option-delete btn btn-cancel" onClick={() => this.onDeleteQuestion(i)}>-</button>
                            </div>
                        </li>
                    })}
                </ul>
                <button className="btn btn-submit" onClick={this.onAddQuestion}>+</button>
                <div style={{'margin-bottom': '4em'} as CSSProperties}></div>
                <button className="btn btn-submit" onClick={this.onCreate}>Create</button>
            </div>
        </div>;
    }
}
