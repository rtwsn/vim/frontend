import React, {ChangeEvent, Component} from "react";
import * as H from "history";
import {PaginatedResponse} from "../../services/api";
import {AxiosError} from "axios";
import "./Surveys.sass"
import {Pagination} from "../Pagination";
import {NavLink} from "react-router-dom";
import {Store} from "redux";
import {ApplicationState} from "../../store";
import {ReactReduxContext} from "react-redux";

export interface SurveyModel {
    id: string
    question: string
    starts: string
    ends: string
    archived: boolean
}

type SurveysState = {
    loading: boolean
    error?: string
    surveys: SurveyModel[]
    page: number
    total: number
    page_size: number
    archived: boolean
    ended: boolean
    future: boolean
}

type SurveysProps = {
    history: H.History
    onListSurveys: (page: number, archived: boolean, ended: boolean, future: boolean) => Promise<PaginatedResponse<SurveyModel>>
    onSetSelection: (max: number) => void
    visualMode: boolean
    selected: number
}

export class Surveys extends Component<SurveysProps, SurveysState> {

    constructor(props: Readonly<SurveysProps>) {
        super(props)
        this.onReload = this.onReload.bind(this)
        this.onSelect = this.onSelect.bind(this)
        this.onPageSelect = this.onPageSelect.bind(this)
        this.props.onSetSelection(-1)
    }

    componentWillMount() {
        this.setState({
            loading: false,
            error: undefined,
            surveys: [],
            page: -1,
            total: -1,
            page_size: -1,
            archived: false,
            ended: true,
            future: false,
        })
    }

    componentDidMount() {
        this.onReload()
    }

    onReload(page: number = this.state.page) {
        this.setState({
            loading: true
        })
        this.props.onListSurveys(page, this.state.archived, this.state.ended, this.state.future).then(result => {
            this.setState({
                loading: false,
                error: undefined,
                surveys: result.data.items,
                page: result.data.meta.page,
                total: result.data.meta.total,
                page_size: result.data.meta.max_results,
            })
            this.props.onSetSelection(result.data.items.length - 1)
        }).catch((err: AxiosError) => {
            this.setState({
                loading: false,
                error: err.message,
            })
        })
    }

    onVisualLeft() {
        this.onPageSelect(this.state.page - 1)
    }

    onVisualRight() {
        this.onPageSelect(this.state.page + 1)
    }

    onVisualSelect(i: number) {
        this.onSelect((this.state.surveys[i] || {}).id)
    }

    onSelect(id: string) {
        if (id !== undefined) {
            this.props.history.push(`/survey/${id}`)
        }
    }

    onPageSelect(page: number) {
        if (page < 0 || page >= Math.floor(this.state.total / this.state.page_size)) {
            return
        }
        this.onReload(page)
    }

    onFilter(e: ChangeEvent, filter: 'ended' | 'future' | 'archived') {
        const state: any = {}
        state[filter] = (e.target as HTMLInputElement).checked
        this.setState(state, () => {
            this.onReload()
        })
    }

    render() {
        return <ReactReduxContext.Consumer>
            {({ store }: { store: Store<ApplicationState> }) => <div className="Surveys-container">
                <h2>Surveys</h2>
                <div className="Surveys-reload">
                    { this.state.loading ?
                        <div className="Loading-content"><div className="Loading-indicator"></div></div>
                        :
                        <button className="btn btn-submit" onClick={() => this.onReload()}>Reload</button>
                    }
                </div>
                {
                    store.getState().auth.permissions.indexOf('create') >= 0 &&
                        <div className="Surveys-controls">
                            <NavLink to="/survey/add"><button className="btn btn-submit">Add</button></NavLink>
                        </div>
                }
                <div className="Surveys-filter">
                    <div className="checkbox inline">
                        <input id="Survey-controls-archived" type="checkbox" checked={this.state.archived} onChange={(e) => this.onFilter(e, 'archived')}/>
                        <label htmlFor="Survey-controls-archived">Show archived</label>
                    </div>
                    <div className="checkbox inline">
                        <input id="Survey-controls-ended" type="checkbox" checked={this.state.ended} onChange={(e) => this.onFilter(e, 'ended')}/>
                        <label htmlFor="Survey-controls-ended">Show ended</label>
                    </div>
                    <div className="checkbox inline">
                        <input id="Survey-controls-future" type="checkbox" checked={this.state.future} onChange={(e) => this.onFilter(e, 'future')}/>
                        <label htmlFor="Survey-controls-future">Show future</label>
                    </div>
                </div>
                {
                    this.state.error !== undefined ?
                        <div className="warn">{this.state.error}</div>
                        :
                        this.state.total === 0 ?
                            <div className="info">No active surveys at the momement.</div>
                            :
                            [
                                <Pagination onSelect={this.onPageSelect} page_size={this.state.page_size} total={this.state.total} page={this.state.page} key="pagination" />,
                                <ul className="Surveys-list" key="surveys-list">
                                        {this.state.surveys.map((survey, i) => {
                                                return <li className={`Surveys-listitem ${this.props.visualMode && this.props.selected === i ? 'selected' : ''} ${survey.archived ? 'archived' : ''}`} onClick={() => this.onSelect(survey.id)} key={i}>{survey.question}</li>
                                            })}
                                </ul>
                            ]
                }
            </div>}
        </ReactReduxContext.Consumer>;
    }
}
