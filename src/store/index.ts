import {applyMiddleware, combineReducers, createStore, Store} from "redux";
import {authReducer} from "./auth/reducers";
import {appReducer} from "./app/reducers";
import { persistStore, persistReducer } from 'redux-persist'
import thunk from "redux-thunk";
import storage from 'redux-persist/lib/storage'
import sessionStorage from 'redux-persist/lib/storage/session'
import {Persistor} from "redux-persist/es/types";

const rootReducer = combineReducers({
    auth: persistReducer({
        key: 'survey.auth',
        storage: sessionStorage,
    }, authReducer),
    app: persistReducer({
        key: 'survey.app',
        storage: storage,
    }, appReducer),
})

export type ApplicationState = ReturnType<typeof rootReducer>

export function configureStore(): {
    store: Store<ApplicationState>,
    persistor: Persistor,
}{
    const store = createStore(rootReducer, applyMiddleware(thunk));
    const persistor = persistStore(store)
    return { store, persistor }
}
