export interface AppState {
    language: string
    theme: 'dark' | 'light'
}

export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE'
export const CHANGE_THEME = 'CHANGE_THEME'

interface ChangeLanguageAction {
    type: typeof CHANGE_LANGUAGE
    language: string
}

interface ChangeThemeAction {
    type: typeof CHANGE_THEME
    theme: 'dark' | 'light'
}

export type AppActionTypes = ChangeLanguageAction | ChangeThemeAction
