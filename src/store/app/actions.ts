import { CHANGE_LANGUAGE, CHANGE_THEME, AppActionTypes } from './types'

export function changeLanguage(newLanguage: string): AppActionTypes {
    return {
        type: CHANGE_LANGUAGE,
        language: newLanguage
    }
}

export function changeTheme(newTheme: 'dark' | 'light'): AppActionTypes {
    return {
        type: CHANGE_THEME,
        theme: newTheme
    }
}

