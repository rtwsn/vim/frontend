import {
    AppState,
    AppActionTypes,
    CHANGE_LANGUAGE,
    CHANGE_THEME
} from './types'

const initialState: AppState = {
    language: 'en',
    theme: 'light',
}

export function appReducer(
    state = initialState,
    action: AppActionTypes
): AppState {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            return {
                language: action.language,
                theme: state.theme,
            }
        case CHANGE_THEME:
            return {
                language: state.language,
                theme: action.theme,
            }
        default:
            return state
    }
}
