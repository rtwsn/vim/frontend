import { AuthState, UPDATE_STATE, AuthActionTypes } from './types'

export function updateSession(newAuthState: AuthState | any): AuthActionTypes {
    return {
        type: UPDATE_STATE,
        payload: newAuthState
    }
}
