import {
    AuthState,
    AuthActionTypes,
    UPDATE_STATE,
} from './types'

const initialState: AuthState = {
    token: '',
    loggedIn: false,
    email: '',
    name: '',
    permissions: [],
}

export function authReducer(
    state = initialState,
    action: AuthActionTypes
): AuthState {
    switch (action.type) {
        case UPDATE_STATE: {
            return {
                ...state,
                ...action.payload
            }
        }
        default:
            return state
    }
}
