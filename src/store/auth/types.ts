export interface AuthState {
    token: string
    loggedIn: boolean
    name: string
    email: string
    permissions: string[]
}

export const UPDATE_STATE = 'UPDATE_STATE'

interface UpdateSessionAction {
    type: typeof UPDATE_STATE
    payload: AuthState
}

export type AuthActionTypes = UpdateSessionAction
