import axios, {AxiosError, AxiosInstance, AxiosResponse} from "axios"
import {Store} from "redux";
import {ApplicationState} from "../store";
import {updateSession} from "../store/auth/actions";
import {BallotModel} from "../views/survey/Survey";
import {SurveyCreation} from "../views/survey/AddSurvey";

export interface PaginatedResponse<T> extends AxiosResponse<Pagination<T>> {

}

export interface Pagination<T> {
    items: Array<T>
    meta: {
        results: number
        max_results: number
        total: number
        page: number
    }
}

export default class ApiService {
    private client: AxiosInstance
    private store: Store<ApplicationState>
    private token?: string

    constructor(baseUrl: string, store: Store<ApplicationState>) {
        this.client = axios.create({
            baseURL: baseUrl,
            transformRequest: [(data, headers) => {
                headers['Authorization'] = this.auth()
                return JSON.stringify(data)
            }],
            headers: {
                'Content-Type': 'application/json'
            },
            timeout: 120000,
        })
        this.store = store
        store.subscribe(() => {
            this.token = this.store.getState().auth.token
        })
    }

    private auth() {
        if (!this.token) {
            return undefined
        }
        return `Bearer ${this.token}`
    }

    private on403() {
        this.store.dispatch(updateSession({
            loggedIn: false
        }))
    }

    public login(email: string, password: string) {
        return new Promise<boolean>((resolve, reject) => {
            this.client.post<{ token: string, email: string, name: string, permissions: string[] }>('/auth/login', {
                email,
                password,
            }).then(result => {
                this.token = result.data.token
                this.store.dispatch(updateSession({
                    name: result.data.name,
                    email: result.data.email,
                    token: result.data.token,
                    permissions: result.data.permissions,
                    loggedIn: true
                }))
                resolve(true)
            }).catch((err: AxiosError) => {
                if (err.response && (err.response as AxiosResponse).status === 403) {
                    this.on403()
                }
                resolve(false)
            })
        })
    }

    public logout() {
        return new Promise<boolean>((resolve, reject) => {
            this.client.delete('/auth/logout').then(result => {
                if (result.status === 200) {
                    return true
                } else {
                    return false
                }
            }).catch((err: AxiosError) => {
                return false
            }).then((result: boolean) => {
                this.store.dispatch(updateSession({
                    token: undefined,
                    loggedIn: false,
                }))
                resolve(true)
            })
        })
    }

    public listSurveys(page: number, archived: boolean, ended: boolean, future: boolean) {
        return this.client.get(
            `/survey/?page=${Number(page)}${!!archived ? '&archived=true' : ''}${!ended ? '&ended=false' : ''}${!!future ? '&future=true' : ''}`
        ).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public getSurvey(id: string) {
        return this.client.get(`/survey/${encodeURI(id)}`).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public submitVote(id: string, ballot: BallotModel) {
        return this.client.put(`/survey/${encodeURI(id)}/vote`, ballot).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public solveChallenge(id: string, ballot: BallotModel) {
        return this.client.post(`/survey/${encodeURI(id)}/vote`, ballot).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public createSurvey(survey: SurveyCreation) {
        return this.client.put(`/survey`, survey).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public tallySurvey(id: string) {
        return this.client.post(`/survey/${encodeURI(id)}/tally`).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

    public archiveSurvey(id: string) {
        return this.client.post(`/survey/${encodeURI(id)}/archive`).catch((err: AxiosError) => {
            if (err.response && (err.response as AxiosResponse).status === 403) {
                this.on403()
            }
            throw err
        })
    }

}
